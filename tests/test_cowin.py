import datetime
from unittest.mock import Mock, patch

import pytest

from cowin.constants import DATE_FORMAT
from cowin.exceptions import BadRequest, ServerError


def test_get_list_of_states_success(httpx_mock, cowin_client):
    states = {
        "states": [{
            "state_id": 58,
            "state_name": "Andaman and Nicobar Islands",
            "state_name_l": ""
        }],
        "ttl": 0
    }
    httpx_mock.add_response(
        status_code=200,
        json=states
    )
    response = cowin_client.get_states()
    assert "states" in response
    assert len(response['states']) == 1
    assert response['states'][0]['state_id'] == 58


def test_get_list_of_states_bad_request(httpx_mock, cowin_client):
    exception_msg = "Bad Request"
    httpx_mock.add_response(
        status_code=400,
        data=exception_msg
    )
    with pytest.raises(BadRequest, match=exception_msg):
        cowin_client.get_states()


def test_get_list_of_states_server_error(httpx_mock, cowin_client):
    exception_msg = "something went wrong in server"
    httpx_mock.add_response(
        status_code=500,
        data=exception_msg
    )
    with pytest.raises(ServerError, match=exception_msg):
        cowin_client.get_states()


def test_get_districts(httpx_mock, cowin_client):
    state_id = 16
    districts = {
        "districts": [
            {
                "state_id": state_id,
                "district_id": 391,
                "district_name": "Ahmednagar",
                "district_name_l": ""
            }
        ]
    }
    httpx_mock.add_response(
        status_code=200,
        json=districts
    )
    response = cowin_client.get_districts(state_id)
    assert "districts" in response
    assert len(response['districts']) == 1
    assert response['districts'][0]['state_id'] == state_id
    assert response['districts'][0]['district_id'] == 391


def test_get_otp_valid_mobile_number(httpx_mock, cowin_client):
    mobile = "9876543210"
    txn_id = "3fa85f64-5717-4562-b3fc-2c963f66afa6"
    httpx_mock.add_response(
        status_code=200,
        json={'txnId': txn_id}
    )
    response = cowin_client.get_otp(mobile)
    assert "txnId" in response
    assert response['txnId'] == txn_id


def test_get_otp_with_invalid_mobile(cowin_client):
    with pytest.raises(BadRequest, match="Invalid mobile number"):
        cowin_client.get_otp("1234567890")


def test_confirm_otp_with_valid_otp(httpx_mock, cowin_client):
    otp = 123456
    mock_response = {
        'token': 'server generated token'
    }
    httpx_mock.add_response(
        status_code=200,
        json=mock_response
    )
    response = cowin_client.confirm_otp(txn_id="abc-def", otp=otp)
    assert "token" in response
    assert response['token'] == "server generated token"


def test_available_sessions_by_pincode(httpx_mock, cowin_client):
    pincode = "605001"
    mock_response = {
        "sessions": [
            {
                "center_id": "1234",
                "name": "Govt. Hospital",
                "address": "123, X Road",
                "state_name": "TamilNadu",
                "district_name": "Chennai",
                "pincode": pincode,
                "lat": "",
                "lon": "",
                "fee_type": "Free",
                "fee": 0,
                "date": datetime.date.today().strftime(DATE_FORMAT),
                "min_age_limit": 18,
                "vaccine": "COVAXIN",
                "slots": [],
                "from": "",
                "to": ""
            }
        ]
    }
    httpx_mock.add_response(
        status_code=200,
        json=mock_response
    )
    response = cowin_client.get_available_sessions_by_pincode(pincode)
    assert "sessions" in response
    assert len(response['sessions']) == 1
    resp_sessions = response['sessions'][0]
    assert resp_sessions['fee_type'] == 'Free'
    assert resp_sessions['min_age_limit'] == 18
    assert resp_sessions['vaccine'] == 'COVAXIN'


def test_get_certificate(httpx_mock, cowin_client):
    beneficiary_id = "1234567890"
    mock_content = b'certificate'
    mock_header = {'content-type': 'application/pdf'}
    httpx_mock.add_response(
        status_code=200,
        data=mock_content,
        headers=mock_header
    )
    response = cowin_client.get_certificate(
        token="AbcDxyzpef",
        beneficiary_id=beneficiary_id
    )
    assert isinstance(response, bytes)
    assert response == mock_content


def test_get_certificate_no_pdf(httpx_mock, cowin_client):
    beneficiary_id = "1234567890"
    httpx_mock.add_response(
        status_code=200,
        data="test content",
        headers={'content-type': 'text/plain'}
    )
    with pytest.raises(ServerError, match="Content is not PDF"):
        cowin_client.get_certificate(
            token="AbcDxyzPef",
            beneficiary_id=beneficiary_id
        )
