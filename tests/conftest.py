import pytest

from cowin import Cowin, AsyncCowin


@pytest.fixture(scope="session")
def cowin_client():
    client = Cowin()
    return client


@pytest.fixture(scope="session")
def async_client():
    client = AsyncCowin()
    return client
