from cowin import utils


def test_valid_mobile_numbers():
    numbers = [
        '9876543210',
        '6789054321',
        '7689054321',
        '8877665544'
    ]
    for number in numbers:
        assert utils.is_mobile_number_valid(number) is True


def test_invalid_mobile_numbers():
    numbers = [
        '1234567890',
        '2345678901,'
        '3344556677',
        '4567890123',
        '5432109876'
    ]
    for number in numbers:
        assert utils.is_mobile_number_valid(number) is False


def test_hash_otp():
    otp = 123456
    assert len(utils.hash_otp(otp)) == 64
