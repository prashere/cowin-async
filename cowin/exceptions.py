class BadRequest(Exception):
    pass


class ServerError(Exception):
    pass
